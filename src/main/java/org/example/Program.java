package org.example;

import java.util.List;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
//        ProductDAO test = new ProductDAO();
////        test.readAll().forEach(product-> System.out.println(product.toString()));
//        System.out.println(test.read("2"));
//        Product product = new Product("May tinh",5000000);
////        test.add(product);
////        test.delete(9);
//        test.update(new Product(5,"Cay luoc",15000));
        int inputLength = args.length;
        if (inputLength > 3 || inputLength < 1) {
            System.err.println("This is lab2 requirement \n" +
                    "Usage: java DatabaseConnection <database_url> <username> <password> or just <database_url>");
            System.exit(1);
        }
        String databaseUrl;
        String username; // default is "root"
        String password;// default is null
        ProductDAO test = new ProductDAO(args[0]);
        // Get the command-line arguments
        switch (inputLength) {
            case 2:
                databaseUrl = args[0];
                username = args[1];
                test = new ProductDAO(databaseUrl, username);
                break;
            case 3:
                databaseUrl = args[0];
                username = args[1];
                password = args[2];
                test = new ProductDAO(databaseUrl, username, password);
                break;
        }


        int n;
        do {
            System.out.println("1. Read all products \n" +
                    "2. Read detail of product by id\n" +
                    "3. Add a new product\n" +
                    "4. Update a product\n" +
                    "5. Delete a product\n" +
                    "6. Exit\n");
            System.out.print("Your choice: ");
            Scanner scanner = new Scanner(System.in);
            n = scanner.nextInt();
            int id;
            int price;
            String name;
            switch (n) {
                case 1:
                    List productList = test.readAll();
                    productList.forEach(product -> System.out.println(product));
                    n = asking();
                    break;
                case 2:

                    System.out.print("id: ");
                    id = scanner.nextInt();
                    System.out.println(test.read(id));
                    n = asking();
                    break;
                case 3:
                    System.out.print("New product name: ");
                    name = scanner.nextLine();
                    System.out.print("New product price: ");
                    price = scanner.nextInt();
                    test.add(new Product(name, price));
                    n = asking();
                    break;
                case 4:
                    System.out.print("id: ");
                    id = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("New name values: ");
                    name = scanner.nextLine();
                    System.out.print("New product price: ");
                    price = scanner.nextInt();
                    test.update(new Product(id, name, price));
                    n = asking();
                    break;

                case 5:
                    System.out.print("id: ");
                    id = scanner.nextInt();
                    test.delete(id);
                    n = asking();
                    break;

            }
        } while (n != 6);

    }

    private static int asking() {  // Asking user if the want to continue
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ban co muon tiep tuc? Y/N: ");
        String option = scanner.nextLine();
        if (option.equals("n") || option.equals("N")) {
            return 6;
        } else {
            return 0;
        }
    }
}
