package org.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static java.sql.DriverManager.getConnection;

public class ProductDAO implements Repository<Product,Integer> {
    private static String DB_URL; //= "jdbc:mysql://localhost:3306/product_management";
    private static String USER_NAME ;
    private static String PASSWORD;

    public ProductDAO(String dbURL, String username, String password) {
        DB_URL = dbURL;
        USER_NAME = username;
        PASSWORD = password;
    }
    public ProductDAO(String dbURL, String username) {
        DB_URL = dbURL;
        USER_NAME = username;
        PASSWORD = null;
    }
    public ProductDAO(String dbURL) {
        DB_URL = dbURL;
        USER_NAME = "root";
        PASSWORD = null;
    }



    @Override
    public Integer add(Product item) {
        Product product = (Product) item;
        try {

            // connnect to database 'testdb'
            Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);
            // crate statement
            PreparedStatement statement = conn.prepareStatement("insert into product(name, price) values (?,?)");
            // get data from tablme 'student'
            statement.setString(1, product.getName());
            statement.setInt(2, product.getPrice());
            int rows = statement.executeUpdate();
            if (rows > 0) {
                System.out.println("Your Data was inserted");
            }

            // close connection
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return product.getId();
    }

    @Override
    public List readAll() {
        ArrayList<Product> productList = new ArrayList<Product>();
        try {

            // connnect to database 'testdb'
            Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);
            // crate statement
            Statement stmt = conn.createStatement();
            // get data from tablme 'student'
            ResultSet rs = stmt.executeQuery("select * from product");
            // show data
            while (rs.next()) {
                Product product = new Product(rs.getInt(1), rs.getString(2), rs.getInt(3));
                productList.add(product);
            }
            System.out.println("Data was get from database success");
            // close connection
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return productList;
    }

    @Override
    public Product read(Integer id) {
        Product product = new Product();
        try {

            // connnect to database 'testdb'
            Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);
            // crate statement
            Statement stmt = conn.createStatement();
            // get data from tablme 'student'
            ResultSet rs = stmt.executeQuery("select * from product where id= " + id);
            // show data
            while (rs.next()) {
                product.setId(rs.getInt(1));
                product.setName(rs.getString(2));
                product.setPrice(rs.getInt(3));
            }
            System.out.println("Data was get from database success");
            // close connection
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return product;
    }

    @Override
    public boolean update(Product item) {
        Product product = (Product) item;
        try {

            // connnect to database 'testdb'
            Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);
            // crate statement
            PreparedStatement statement = conn.prepareStatement("update product set name=?, price = ? where id = ?" );
            // get data from tablme 'student'
            statement.setInt(3, product.getId());
            statement.setString(1, product.getName());
            statement.setInt(2, product.getPrice());
            int rows = statement.executeUpdate();
            if (rows > 0) {
                System.out.println("Your product was updated successfully !");
            }

            // close connection
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            // connnect to database 'testdb'
            Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);
            // crate statement
            PreparedStatement statement = conn.prepareStatement("delete from product where id = " + id);
            int rows = statement.executeUpdate();
            if (rows > 0) {
                System.out.println("Your product was delete successfully");
                conn.close();
                return true;

            } else {
                conn.close();
                return false;
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        // close connection
    }
}
